/**
 * Created by Dmitry Vereykin on 7/26/2015. Honestly asked for help because I don't understand money topics well.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;

public class TravelExpenses extends JFrame{
    private JPanel panel;
    private JTextField numberOfDaysField;
    private JTextField airfareField;
    private JTextField carRentalFeesField;
    private JTextField milesDrivenField;
    private JTextField parkingFeesField;
    private JTextField taxiChargesField;
    private JTextField registrationFeesField;
    private JTextField lodgingChargesField;

    public TravelExpenses(){
        setTitle("Total Travel Expenses");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buildPanel();
        add(panel);

        setSize(450, 300);
        setVisible(true);
        setLocationRelativeTo(null);
   
   }

   private void buildPanel(){
       JLabel numberOfDaysMsg = new JLabel(" Days on Trip: ");
       numberOfDaysField = new JTextField("0", 10);
       JLabel airfareMsg = new JLabel(" Airfare (Optional): ");
       airfareField = new JTextField("0", 10);
       JLabel carRentalFeesMsg = new JLabel(" Car Rental (Optional): ");
       carRentalFeesField = new JTextField("0", 10);
       JLabel milesDrivenMsg = new JLabel(" Miles Driven (Private Vehicles ONLY): ");
       milesDrivenField = new JTextField("0", 10);
       JLabel parkingFeesMsg = new JLabel(" Parking Ticket Fees (Optional): ");
       parkingFeesField = new JTextField("0", 10);
       JLabel taxiChargesMsg = new JLabel(" Taxi Charges (Optional): ");
       taxiChargesField = new JTextField("0", 10);
       JLabel conferenceFeesMsg = new JLabel(" Conference Fees (Optional)");
       registrationFeesField = new JTextField("0", 10);
       JLabel lodgingChargesMsg = new JLabel(" Lodging Charges Per Night: ");
       lodgingChargesField = new JTextField("0", 10);
       JLabel justMsg = new JLabel("            ");

       JButton calcButton = new JButton("Calculate Expenses");
       calcButton.addActionListener(new CalcButtonListener());

       panel = new JPanel();
       setLayout(new GridLayout(9, 2));

       add(numberOfDaysMsg);
       add(numberOfDaysField);
       add(airfareMsg);
       add(airfareField);
       add(carRentalFeesMsg);
       add(carRentalFeesField);
       add(milesDrivenMsg);
       add(milesDrivenField);
       add(parkingFeesMsg);
       add(parkingFeesField);
       add(taxiChargesMsg);
       add(taxiChargesField);
       add(conferenceFeesMsg);
       add(registrationFeesField);
       add(lodgingChargesMsg);
       add(lodgingChargesField);
       add(justMsg);
       panel.add(calcButton);

   }

   private class CalcButtonListener implements ActionListener{
       public void actionPerformed(ActionEvent e){
           int days = Integer.parseInt(numberOfDaysField.getText());
           int milesDriven = Integer.parseInt(milesDrivenField.getText());

           double airfare = Double.parseDouble(airfareField.getText());
           double carRentalFees = Double.parseDouble(carRentalFeesField.getText());
           double parkingFees = Double.parseDouble(parkingFeesField.getText());
           double taxiCharges = Double.parseDouble(taxiChargesField.getText());
           double conferenceFees = Double.parseDouble(registrationFeesField.getText());
           double lodgingCharges = Double.parseDouble(lodgingChargesField.getText());

           double totalIncurred;
           double totalAllocated;
           double totalAllocatedMeals;
           double totalAllocatedParking;
           double totalAllocatedLodging;
           double totalAllocatedTaxi;
           double totalAllocatedPCar;
           double savedOnParking;
           double savedOnTaxi;
           double savedOnLodging;
           double savedOnPCar;

           double parkingIncurred = parkingFees * days;
           double taxiIncurred = taxiCharges * days;
           double lodgingIncurred = lodgingCharges * days;
           double privateCarIncurred = carRentalFees * milesDriven;

           DecimalFormat dollar = new DecimalFormat("#,##0.00");
           totalIncurred = airfare + privateCarIncurred +
                			parkingIncurred + taxiIncurred +
            	       	conferenceFees + lodgingIncurred;

           totalAllocatedMeals = (double)days * 37.00;
           totalAllocatedParking = (double)days * 10.00;
           totalAllocatedTaxi = (double)days * 20.00;
           totalAllocatedLodging = (double)days * 95.00;
           totalAllocatedPCar = (double)milesDriven * 0.27;

           totalAllocated = totalAllocatedMeals + totalAllocatedParking +
                   totalAllocatedLodging + totalAllocatedTaxi + totalAllocatedPCar;

           savedOnParking = totalAllocatedParking - parkingIncurred;
           savedOnTaxi = totalAllocatedTaxi - taxiIncurred;
           savedOnLodging = totalAllocatedLodging - lodgingIncurred;
           savedOnPCar = totalAllocatedPCar - privateCarIncurred;

           JOptionPane.showMessageDialog(null, "Total Expenses Incurred: $" +
            			dollar.format(totalIncurred) +
            			"\nTotal Expenses Allocated: $" + 
            			dollar.format(totalAllocated) +
            			amountSaved(savedOnParking, savedOnTaxi, savedOnLodging, savedOnPCar));
      	
      }
      
      private String amountSaved(double parking, double taxi, double lodging, double vehicle){
         double amountSaved = parking + taxi + lodging + vehicle;
         DecimalFormat dollar = new DecimalFormat("#,##0.00");

         if (amountSaved >= 0){  
            return "\nTotal Amount Saved: $" + dollar.format(amountSaved);
         }else{
            return "\nTotal Amount Due: $" + dollar.format(amountSaved * -1);
         }
      }

   }

   public static void main (String[] args){
      new TravelExpenses();
   }

}

